package org.afpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonProjetTest2Application {

	public static void main(String[] args) {
		SpringApplication.run(MonProjetTest2Application.class, args);
	}

}
